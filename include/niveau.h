#ifndef NIVEAU // if not defini,
#define NIVEAU //  tu le défnis
#include <vector>
#include <string>
#include "rectangle.h"


void fillNiveau(std::vector<Player>& players,std::vector<Decor>& decor, std::vector<End>& endPosition,std::string fileName);
void displayLevel(std::vector<Decor> decor, std::vector<End>& endPosition);

#endif