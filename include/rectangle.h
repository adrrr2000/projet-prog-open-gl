#ifndef RECTANGLE_H
#define RECTANGLE_H

#include <SDL2/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdlib.h>
#include <stdio.h>
#include <vector>



/*############## CLASSE DES ÉLÉMENTS DE DECOR #################*/

class Decor
{
    public: 

    float width, height;
    float x,y;
    float color1, color2, color3;

    public: 


    Decor(float width,float height,float x, float y,float color1, float color2, float color3);
    void draw();
};




/*############## CLASSE DES JOUEURS #################*/

class Player
{
    public: 

    float width, height;
    float x,y;
    float xVel, yVel;
    float jumpCapacity;
    float xFinish, yFinish;
    float color1, color2, color3;
    int canJump,filled, onGround;

    public: 

    //initialisation du joueur
    Player(float width,float height,float x, float y, float jumpCapacity, float xFinish, float yFinish, float color1, float color2, float color3);
    void place(std::vector<Decor> niveau ,std::vector<Player> players);
    void draw();
    void reactInputs(SDL_Event event);
    bool collisionBack(const Decor& decor);
    void resetPosition(float x0, float y0);
    bool collisionGround(const Decor& decor);
    //(const Decor& decor);
    bool collisionFront(const Decor& decor);
    bool jumpOnOther(const Player& players);
    bool playerEnds();
};

/*##############CLASSE DES POSITIONS DE FIN DE NIVEAU#################*/

class End
{
    public: 

    float width, height;
    float x,y;
    float color1, color2, color3;

    public: 


    End(float width,float height,float x, float y,float color1, float color2, float color3);
    void draw();
    
};


#endif



