#include <SDL2/SDL.h>
#include "fakesdlimage.h"
#include <GL/gl.h>
#include <GL/glu.h> 
#include <stdlib.h>
#include <stdio.h>


#include "texture.h"

GLuint chargeImageTexture(char* fileName){
    // Chargement images
    SDL_Surface* image = IMG_Load(fileName);
    // on vérifie que l'image est bien chargée
    if (image == NULL)
    {
        printf("Ton image ne veut pas se charger \n");
    }
    //on crée la texture
    GLuint texture;
    //on l'initialise
    glGenTextures(1, &texture); 
    // on fixe les paramètres de la texture
    // Attachez la texture au point de bind GL_TEXTURE_2D , en utilisant glBindTexture
    glBindTexture(GL_TEXTURE_2D, texture);
    // Changez le filtre de minification en utilisant la fonction glTexParameteri
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    //Envoyez les données de l'image vers le GPU, en utilisant la fonction glTexImage2D
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image->w, image->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, image->pixels);
    // détacher la texture de son point de bind, une fois les données chargées
    glBindTexture(GL_TEXTURE_2D, 0);
    SDL_FreeSurface(image);
    return texture;

}

void displayTextureLevel(GLuint textureLevel){
    glColor3f(1.0,1.0,1.0);
            // texture
            // j'active la texture
            glEnable(GL_TEXTURE_2D); 

            //pour la transparence
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

            // Attachez la texture au point de bind
            glBindTexture(GL_TEXTURE_2D, textureLevel);


            //je dessine un quad
            glBegin(GL_QUADS);
                glTexCoord2f(0, 0);
                glVertex2f(-400,350);
                glTexCoord2f(1, 0);
                glVertex2f(-200,350);
                glTexCoord2f(1, 1);
                glVertex2f(-200,150);
                glTexCoord2f(0, 1); 
                glVertex2f(-400,150);
                glEnd();


            // détacher la texture, après le dessin
            glBindTexture(GL_TEXTURE_2D, 0);

            // désactiver le texturing après le dessin
            glDisable(GL_TEXTURE_2D);
}

void displayTextureMenu(GLuint textureMenu){
    glColor3f(1.0,1.0,1.0);
            // texture
            // j'active la texture
            glEnable(GL_TEXTURE_2D); 

            //pour la transparence
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

            // Attachez la texture au point de bind
            glBindTexture(GL_TEXTURE_2D, textureMenu);


            //je dessine un quad
            glBegin(GL_QUADS);
                glTexCoord2f(0, 0);
                glVertex2f(-385,310);
                glTexCoord2f(1, 0);
                glVertex2f(-210,310);
                glTexCoord2f(1, 1);
                glVertex2f(-210,135);
                glTexCoord2f(0, 1); 
                glVertex2f(-385,135);
                glEnd();


            // détacher la texture, après le dessin
            glBindTexture(GL_TEXTURE_2D, 0);

            // désactiver le texturing après le dessin
            glDisable(GL_TEXTURE_2D);
}

void displayAccueil(bool accueil, GLuint texture){
    
    if (accueil)
    {
        glColor3f(1.0,1.0,1.0);
            // texture
            // j'active la texture
            glEnable(GL_TEXTURE_2D); 

            //pour la transparence
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

            // Attachez la texture au point de bind
            glBindTexture(GL_TEXTURE_2D, texture);


            //je dessine un quad
            glBegin(GL_QUADS);
                glTexCoord2f(0, 0);
                glVertex2f(-500,500);
                glTexCoord2f(1, 0);
                glVertex2f(500,500);
                glTexCoord2f(1, 1);
                glVertex2f(500,-500);
                glTexCoord2f(0, 1); // Appliquez les coordonnées de texture sur les sommets du quad
                glVertex2f(-500,-500);
                glEnd();

            // détacher la texture, après le dessin
            glBindTexture(GL_TEXTURE_2D, 0);

            // désactiver le texturing après le dessin
            glDisable(GL_TEXTURE_2D);
    }
    
}

void displayMenu(bool menu, GLuint texture){
    
    if (menu)
    {
        glColor3f(1.0,1.0,1.0);
            // texture
            // j'active la texture
            glEnable(GL_TEXTURE_2D); 

            //pour la transparence
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

            // Attachez la texture au point de bind
            glBindTexture(GL_TEXTURE_2D, texture);


            //je dessine un quad
            glBegin(GL_QUADS);
                glTexCoord2f(0, 0);
                glVertex2f(-400,400);
                glTexCoord2f(1, 0);
                glVertex2f(400,400);
                glTexCoord2f(1, 1);
                glVertex2f(400,-400);
                glTexCoord2f(0, 1); // Appliquez les coordonnées de texture sur les sommets du quad
                glVertex2f(-400,-400);
                glEnd();

            // détacher la texture, après le dessin
            glBindTexture(GL_TEXTURE_2D, 0);

            // désactiver le texturing après le dessin
            glDisable(GL_TEXTURE_2D);
    }
    
}