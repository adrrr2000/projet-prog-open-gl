#include "rectangle.h"
#include <stdlib.h>
#include <stdio.h>
#include <SDL2/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h> 
#include <iostream>




/*##############CLASSE DES ÉLÉMENTS DE DECOR#################*/

Decor::Decor(float width,float height,float x, float y,float color1, float color2, float color3)
{
    this->height=height; // hauteur
    this->width=width; // largeur
    this->x=x;
    this->y=y; 
    this->color1=color1;
    this->color2=color2;
    this->color3=color3;
}


void Decor::draw()
{
    
    glColor3f(color1,color2,color3);
    glBegin(GL_QUADS);
        glVertex2f(x, y);
        glVertex2f(x+width,y);
        glVertex2f(x+width,y-height);
        glVertex2f(x, y-height);
    glEnd();      
}



/*############## CLASSE DES JOUEURS #################*/


//initialisation du rectangle
Player::Player(float width,float height,float x, float y, float jumpCapacity, float xFinish, float yFinish, float color1, float color2, float color3)
{
    this->height=height; // hauteur
    this->width=width; // largeur
    this->x=x; //position x de départ
    this->y=y; //position y de départ
    this->jumpCapacity = jumpCapacity;
    this->xVel=0;
    this->yVel= -1.;
    this->xFinish=xFinish;
    this->yFinish=yFinish;
    this->color1=color1;
    this->color2=color2;
    this->color3=color3;
    this->canJump = 0; // 1 s'il peut sauter (ie en collision avec le sol), 0 sinon
    this->filled = 0; // 1 s'il est rempli et donc jouable, 0 sinon
    this->onGround = 0; //1 s'il est sur le sol en collision
    // this->onOther = 0;
}


void Player::reactInputs(SDL_Event event)


{
    //yVel = -0.5; //on remet à jour yVel à chaque tour de boucle

    
    if( event.type == SDL_KEYDOWN )
    {
        //ajustement de la vitesse
        switch( event.key.keysym.sym )
        {
            case SDLK_SPACE:
                if (canJump == 1)
                {
                    //std::cout<<"espace"<<std::endl;
                    yVel = jumpCapacity+1.;
                }
            break;


            case SDLK_LEFT: 
                xVel -= 0.1; 
            break;

            case SDLK_RIGHT: 
                xVel += 0.1; 
            break;
            
            default: break;
        }
    }
}


void Player::place(std::vector<Decor> niveau, std::vector<Player> players)
{

    //std::cout<<canJump<<std::endl;

    x += xVel;

    //std::cout<<yVel<<std::endl;
    y += yVel;

    //std::cout<<"pos x = "<<x<<" pos y = "<<y<<std::endl;

    yVel = -1.; //on remet la gravité pour éviter qu'il ne continue à monter après avoir appuyé sur espace
    canJump = 0; //on lui interdit de sauter avant de savoir s'il y a collision avec le sol
    onGround = 0; 


    for (long unsigned int i = 0; i < niveau.size(); i++)
    {
        // for (int i = 0; i<players.size(); i++)
        // {
        if (collisionFront(niveau[i])==true)
        {
            //std::cout<<"collision Avant"<<std::endl;
            x -= xVel;
        }
        if (collisionGround(niveau[i])==true /* || onOther == 1*/)
        {
            y-= yVel;
            canJump = 1;
            onGround = 1;
            // std::cout<<"joueur sur le sol"<<std::endl;

            //std::cout<<onOther<<std::endl;
        }
        // if (collisionCeiling(niveau[i])==true)
        // {
        //     //std::cout<<"y avant collision : "<<y<<std::endl;
        //     y += yVel;
        //     //std::cout<<"y après collision : "<<y<<std::endl;
        //     //std::cout<<"collision Plafond"<<std::endl;
        // }
        if (collisionBack(niveau[i])==true)
        {
            //std::cout<<"collision Arrière"<<std::endl;
            x -= xVel;
        }
     //  }

    }

    for (long unsigned int i = 0; i<players.size(); i++)
    {
        if (jumpOnOther(players[i]) == true && onGround != 1)
        {
            y-=yVel;
            canJump = 1;
            onGround = 0;
            // std::cout<<"joueur sur l'autre"<<std::endl;
        }
    }
}


void Player::draw()
{
    glColor3f(color1,color2,color3);
    if (filled)
    {
        glBegin(GL_QUADS);
    }
    else 
    {
        glBegin(GL_LINE_LOOP);
    }
    glVertex2f(x, y);
    glVertex2f(x+width,y);
    glVertex2f(x+width,y-height-2);
    glVertex2f(x, y-height-2);

    glEnd();
}

//FONCTIONS DE COLLISIONS QUI RETOURNENT TRUE SI LA COLISION EST AVÉRÉE

bool Player::collisionGround(const Decor& decor)
{ 
    if (x + width <decor.x || x> decor.x + decor.width || y-height > decor.y +2 || y < decor.y -decor.height )
    {
        return false;
    }
    else 
    {
        return true;
    }
} 



// bool Player::collisionCeiling(const Decor& decor)
// {
//     if (x+width < decor.x  || x > decor.x + decor.width  || y < decor.y - decor.height -2 || y-height > decor.y-decor.height )
//     {
//         return false;
//     }
//     else
//     {
//         return true;
//     }
// }


bool Player::collisionFront(const Decor& decor)
{
    if (x+width < decor.x -2 || x+width > decor.x + decor.width +2 || y-height > decor.y || y <decor.y - decor.height)
    {
        return false;
    }
    else
    {
        return true;
    }
}

bool Player::collisionBack(const Decor& decor)
{
    if (x > decor.x+decor.width +2 || x+width < decor.x + decor.width - 2 || y-height > decor.y || y < decor.y - decor.height)
    {
        return false;
    }
    else 
    {
        return true;
    }
}

bool Player::playerEnds(){    
    return xFinish-2<x && x<xFinish+2 && yFinish-2<y && y<yFinish+2;
}



bool Player::jumpOnOther(const Player& player)
{
    if (x + width <player.x || x> player.x + player.width || y-height > player.y || y < player.y)
    {
        return false;
    }
    else 
    {
        return true;
    }
}


/*##############CLASSE DES POSITIONS DE FIN DE NIVEAU#################*/

End::End(float width,float height,float x, float y,float color1, float color2, float color3)
{
    this->height=height; // hauteur
    this->width=width; // largeur
    this->x=x;
    this->y=y; 
    this->color1=color1;
    this->color2=color2;
    this->color3=color3;
}


void End::draw()
{
    
    glColor3f(color1,color2,color3);
    glBegin(GL_LINE_LOOP);
        glVertex2f(x, y);
        glVertex2f(x+width,y);
        glVertex2f(x+width,y-height-2);
        glVertex2f(x, y-height-2);
    glEnd();
    
        
}




