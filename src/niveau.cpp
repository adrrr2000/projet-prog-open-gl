#include <SDL2/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdlib.h>
#include <stdio.h>
#include "rectangle.h"
#include "niveau.h"
#include <vector>
#include <sstream>
#include <string>

#include <iostream>
#include <fstream>


using namespace std;

void fillNiveau(std::vector<Player>& players, std::vector<Decor>& decor, std::vector<End>& endPosition,std::string fileName){
    
    string filename(fileName);
    
    string line;
    ifstream input_file(fileName);
    if (!input_file.is_open()) {
        cerr << "Could not open the file - '"
             << filename << "'" << endl;
    }

    float width, height;
    float x,y;
    float jumpCapacity;
    float xFinish, yFinish;
    float color1, color2, color3;


    while (getline(input_file, line)){ // sépare le doc en lignes (je crois)
        
        stringstream lineStream(line);
        
        getline(lineStream, line, '!'); // coupe la ligne en morceau
        // 'line' correspond à partir d'ici au premier élément d'une ligne
        
            //cout<<line<<endl;
        if (line != "joueur" && line != "fin") //on stocke le décor dans le vecteur décor
        {
            lineStream >> width;
            lineStream.ignore();
            lineStream >> height;
            lineStream.ignore();
            lineStream >> x;
            lineStream.ignore();
            lineStream >> y;
            lineStream.ignore();
            lineStream >> color1;
            lineStream.ignore();
            lineStream >> color2;
            lineStream.ignore();
            lineStream >> color3;
            Decor background(width,height,x, y, color1, color2, color3);
            decor.push_back(background);
        }

        if (line=="fin")
        {
            lineStream >> width;
            lineStream.ignore();
            lineStream >> height;
            lineStream.ignore();
            lineStream >> x;
            lineStream.ignore();
            lineStream >> y;
            lineStream.ignore();
            lineStream >> color1;
            lineStream.ignore();
            lineStream >> color2;
            lineStream.ignore();
            lineStream >> color3;
            End fin(width,height,x, y, color1, color2, color3);
            endPosition.push_back(fin);
        }
        
        if (line=="joueur")
        {
            lineStream >> width;
            lineStream.ignore();
            lineStream >> height;
            lineStream.ignore();
            lineStream >> x;
            lineStream.ignore();
            lineStream >> y;
            lineStream.ignore();
            lineStream >> jumpCapacity;
            lineStream.ignore();
            lineStream >> xFinish;
            lineStream.ignore();
            lineStream >> yFinish;
            lineStream.ignore();
            lineStream >> color1;
            lineStream.ignore();
            lineStream >> color2;
            lineStream.ignore();
            lineStream >> color3;
            Player joueurs(width,height,x, y, jumpCapacity, xFinish, yFinish, color1, color2, color3);
            players.push_back(joueurs);
        }

    }
    input_file.close();
    
}

void displayLevel(std::vector<Decor> decor,std::vector<End>& endPosition){
    for (long unsigned int i = 0; i < decor.size(); i++)
        {
            decor[i].draw();
        }
    for (long unsigned int i = 0; i < endPosition.size(); i++)
        {
            endPosition[i].draw();
        }
}




