#include <SDL2/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdlib.h>
#include <stdio.h>
#include "rectangle.h"
#include "camera.h"
#include "niveau.h"
#include "texture.h"
#include <sstream>



#include <iostream>
#include <fstream>

using namespace std;

static const int MAX_PLAYERS = 4;

/* Dimensions initiales et titre de la fenetre */
static const unsigned int WINDOW_WIDTH = 800;
static const unsigned int WINDOW_HEIGHT = 600;
static const char WINDOW_TITLE[] = "Jeu, tout simplement";

/* Espace fenetre virtuelle */
static const float GL_VIEW_SIZE = 600.;

/* Nombre minimal de millisecondes separant le rendu de deux images */
static const Uint32 FRAMERATE_MILLISECONDS = 1000 / 60;


static float aspectRatio;

void onWindowResized(unsigned int width, unsigned int height)
{ 
    aspectRatio = width / (float) height;

    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    if( aspectRatio > 1) 
    {
        gluOrtho2D(
        -GL_VIEW_SIZE / 2. * aspectRatio, GL_VIEW_SIZE / 2. * aspectRatio, 
        -GL_VIEW_SIZE / 2., GL_VIEW_SIZE / 2.);
    }
    else
    {
        gluOrtho2D(
        -GL_VIEW_SIZE / 2., GL_VIEW_SIZE / 2.,
        -GL_VIEW_SIZE / 2. / aspectRatio, GL_VIEW_SIZE / 2. / aspectRatio);
    }
}



int main(int argc, char** argv) 
{
    /* Initialisation de la SDL */

    if(SDL_Init(SDL_INIT_VIDEO) < 0) 
    {
        const char* error = SDL_GetError();
        fprintf(
            stderr, 
            "Erreur lors de l'intialisation de la SDL : %s\n", error);

        SDL_Quit();
        return EXIT_FAILURE;
    }
	
    /* Ouverture d'une fenetre et creation d'un contexte OpenGL */

    SDL_Window* window;
    {
        window = SDL_CreateWindow(
        WINDOW_TITLE,
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
        WINDOW_WIDTH, WINDOW_HEIGHT,
        SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);

        if(NULL == window) 
        {
            const char* error = SDL_GetError();
            fprintf(
                stderr,
                "Erreur lors de la creation de la fenetre : %s\n", error);

            SDL_Quit();
            return EXIT_FAILURE;
        }
    }
    
    SDL_GLContext context;
    {
            SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
            SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);

            SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

        context = SDL_GL_CreateContext(window);
    
        if(NULL == context) 
        {
            const char* error = SDL_GetError();
            fprintf(
                stderr,
                "Erreur lors de la creation du contexte OpenGL : %s\n", error);
            SDL_DestroyWindow(window);
            SDL_Quit();
            return EXIT_FAILURE;
        }
    }    
  
    onWindowResized(WINDOW_WIDTH, WINDOW_HEIGHT);


    // Variables utiles pour la suite
    
    bool level1 = false;
    bool level2 = false;
    bool accueil = true;
    bool menu = false;
    bool nextLevel = false;
    int actualLevel=0; // 0 pour l'accueil, 1 pour niveau 1, 2 pour niveau 2
    int ecart = 0; // ça va servir pour remettre le joueur à sa position initiale au début d'un niveau
    bool pressTab = true;

// textures pour la suite
    GLuint textureAccueil = chargeImageTexture("image/accueil.png");

    GLuint textureLevel1 = chargeImageTexture("image/niveau 1.png");
    GLuint textureLevel2 = chargeImageTexture("image/niveau2.png");
    GLuint textureMenu = chargeImageTexture("image/mPourMenu.png");
    GLuint textureMenuPause = chargeImageTexture("image/menu.png");
    GLuint textureNextLevel = chargeImageTexture("image/niveauSuivant.png");
    

    /* Boucle principale */
    int loop = 1;

    Point cam(0,0); // on initie la caméra au centre du repère
   
    
    //niveau1
    std::vector<Decor> decor; //le vecteur qui va stocker le decor
    std::vector<End> endPosition;
    std::vector<Player> players; //[MAX_PLAYERS] ;
    string fileName;
    fileName = "niveaux/niveau1.txt";
    fillNiveau(players,decor,endPosition,fileName);
    players[0].filled = 0;

    //niveau2
    std::vector<Decor> decor2; //le vecteur qui va stocker le decor
    std::vector<End> endPosition2;
    std::vector<Player> players2; //[MAX_PLAYERS] ;
    string fileName2;
    fileName2 = "niveaux/niveau2.txt";
    fillNiveau(players2,decor2,endPosition2,fileName2);
    players2[0].filled = 1;
    long unsigned int i = 0;
    int actualPlayer = 0;

    //int RightPlacedPlayers = 0;



    while(loop) 
    {
        /* Recuperation du temps au debut de la boucle */
        Uint32 startTime = SDL_GetTicks();

        
        /* Placer ici le code de dessin */
        glClear(GL_COLOR_BUFFER_BIT);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        
        displayAccueil(accueil,textureAccueil); //affiche l'accueil
        displayMenu(menu,textureMenuPause); // affiche le menu pause

     

        if (level1) 
        {
            pressTab = false;
            
            //caméra
            glPushMatrix();
            cam.camera(players[0]);
            displayLevel(decor, endPosition);

            players[0].place(decor,players);
            players[0].draw();



            glPopMatrix();

            // textures affichées en haut à gauche du niveau
            displayTextureLevel(textureLevel1);
            displayTextureMenu(textureMenu);

            if (players[0].playerEnds())
            {
                level1=false;
                level2=true;
                //nextLevel=true;
            }
        }  

        if (level2)
        {
            pressTab = true;          

            //caméra
            glPushMatrix();
            cam.camera(players2[actualPlayer]);
            displayLevel(decor2, endPosition2);
            

            players2[actualPlayer].place(decor2,players2);
            for (long unsigned int j = 0; j<players2.size(); j++)
            {
                players2[j].draw(); 
            }


            glPopMatrix();

            // textures affichées en haut à gauche du niveau
            displayTextureLevel(textureLevel2);
            displayTextureMenu(textureMenu);

            if (players2[0].playerEnds() && players2[1].playerEnds() && players2[2].playerEnds())
            {
                level2=false;
                //nextLevel=true;
            }

            
        }
        
    
            
        
        /* Echange du front et du back buffer : mise a jour de la fenetre */
        SDL_GL_SwapWindow(window);
        
        /* Boucle traitant les evenements */
        SDL_Event e;
        while(SDL_PollEvent(&e)) 
        {
             /* L'utilisateur ferme la fenetre : */
		 	if(e.type == SDL_QUIT) 
		 	{
		 		loop = 0;
		 		break;
		 	}
		
		 	if(	e.type == SDL_KEYDOWN 
		 		&& (e.key.keysym.sym == SDLK_q || e.key.keysym.sym == SDLK_ESCAPE))
		 	{
		 		loop = 0; 
		 		break;
		 	}

            if (level1)
            {
                players[0].reactInputs(e);
            }
            if (level2)
            {
                players2[actualPlayer].reactInputs(e);
            }

            
            switch(e.type) 
            {
                case SDL_WINDOWEVENT:
                    switch (e.window.event) 
                    {
                        /* Redimensionnement fenetre */
                        case SDL_WINDOWEVENT_RESIZED:
                            onWindowResized(e.window.data1, e.window.data2);                
                            break;

                        default:
                            break; 
                    }
                    break;

            //     /* Clic souris */
                case SDL_MOUSEBUTTONUP:
                    printf("clic en (%d, %d)\n", e.button.x, e.button.y);
                    break;
                
                /* Touche clavier */
                case SDL_KEYDOWN:
                    switch (e.key.keysym.sym)
                    {
                    //niveau 1
                        case SDLK_1:
                        if (actualLevel!=2) // on ne peut pas lancer le niveau 1 depuis le niveau 2
                        {
                            level1 = true;
                            accueil=false;
                            menu=false;
                            nextLevel = false;
                            actualLevel=1;
                        }
                            break;

                        case SDLK_KP_1:
                            if (actualLevel!=2) // on ne peut pas lancer le niveau 1 depuis le niveau 2
                            {
                            level1 = true;
                            accueil=false;
                            menu=false;
                            nextLevel = false;
                            actualLevel=1;
                            }
                            break;

                        case SDLK_2: // on ne peut pas lancer le niveau 2 depuis le niveau 1
                            if (actualLevel!=1) 
                            {
                                level2 = true;
                                accueil=false;
                                menu=false;
                                nextLevel = false;
                                level1 = false;
                                actualLevel=2;
                            }
                            break;

                        case SDLK_KP_2: 
                            if (actualLevel!=1) // on ne peut pas lancer le niveau 2 depuis le niveau 1
                            {
                                level2 = true;
                                accueil=false;
                                menu=false;
                                nextLevel = false;
                                level1 = false;
                                actualLevel=2;
                            }
                            break;

                        case SDLK_m: //m pour menu
                            level1 = false;
                            level2=false;
                            nextLevel=false;
                            accueil = false;
                            menu=true;
                            break;

                        case SDLK_r: // le if permet le retour au bon niveau
                            if(actualLevel==1){ 
                                level1 = true;
                                level2=false;
                                nextLevel=false;
                                menu=false;
                            }
                            else
                            {
                                level1 = false;
                                level2=true;
                                nextLevel=false;
                                menu=false;
                            }
                            break;

                        case SDLK_n:
                            level1 = false;
                            level2=true;
                            nextLevel=false;
                            menu=false;
                            break;


                    case SDLK_TAB : 
                        if (pressTab==true && players2[actualPlayer].canJump == 1)
                            {
                                players2[actualPlayer].filled = 0;
                                players2[actualPlayer].xVel = 0;
                                i += 1;
                                actualPlayer = i;
                                if (i>players2.size()-1)
                                {
                                    i = 0;
                                    actualPlayer = 0;
                                }
                                players2[actualPlayer].filled = 1;
                            }
                    default:
                        break;
                    }

                default:
                    break;
            }
        }

        /* Calcul du temps ecoule */
        Uint32 elapsedTime = SDL_GetTicks() - startTime;
        /* Si trop peu de temps s'est ecoule, on met en pause le programme */
        if(elapsedTime < FRAMERATE_MILLISECONDS) 
        {
            SDL_Delay(FRAMERATE_MILLISECONDS - elapsedTime);
        }
    }

    /* Liberation des ressources associees a la SDL */ 
    SDL_GL_DeleteContext(context);
    SDL_DestroyWindow(window);
    SDL_Quit();
    
    return EXIT_SUCCESS;
}
