#ifndef TEXTURE_H // if not defini,
#define TEXTURE_H //  tu le défnis

GLuint chargeImageTexture(char* fileName);
void displayTextureLevel(GLuint textureLevel);
void displayTextureMenu(GLuint textureMenu);
void displayAccueil(bool menu, GLuint texture);
void displayMenu(bool menu, GLuint texture);

#endif